from math import log10, floor
import numpy as np

def round_to_1(x):
    return round(x, -int(floor(log10(abs(x)))))

def nJetsTag(nJets):
    '''
    Make a tag that specifies the number of jets you have
    in the data sample, i.e, 5k for 5000.

    Input:
    Njets: the number of jets in the sample

    Output:
    jetTag (string): The number of jet in one sig fig with the
        units appended.
    '''

    if nJets < 1e3:
        jetTag = "{:.0f}".format(round_to_1(nJets))

    elif nJets < 1e6:
        # Appeding "k" to the file name, round to 1 sig fig
        jetTag = "{:.0f}k".format(round_to_1(nJets / 1e3))

    elif nJets < 1e9:
        jetTag = "{:.0f}m".format(round_to_1(nJets / 1e6))
    elif nJets < 1e12:
        jetTag = "{:.0f}g".format(round_to_1(nJets / 1e9))
    else:
        print('Note; you\'re running with more than a trillion events, are you sure this is what you want to do?')
        jetTag = "{:.0f}t".format(round_to_1(nJets / 1e12))

    return(jetTag)


def strToList(myStr, delimitter=","):
    '''
    Go from a string of a list of inputs to a list of strings

    Input:
    - myStr: Comma separated list of vars (in trk_xr)
    - delimitter: The delimmiter to split the list by
    Output:
    - myList: Comma separated list of strs

    '''

    if myStr == '-1':
        myList = []
    else:
        myList = myStr.replace(" ","").split(delimitter)

    return myList

def getTrkMask(X, mask_value=0):
    '''
    Given an array for the track inputs, return a mask for the valid tracks.

    Inputs:
    - X: np array of shape (nJets, nTrks, nTrkFeatures)
    - mask_value: value corresponding to masked inputs (default 0)

    Output:
    - trk_mask: mask for valid tracks, of shape (nJets, nTrks)

    '''

    trk_mask = (np.sum(X, axis=-1) != mask_value)
    return trk_mask


def VR(pt,rho=30,Rmin=0.02,Rmax=0.4):
    '''
    The alg defining the radius of the jet

    Inputs:
    - pt: pT in [GeV]
    - rho, Rmin, Rmax: The parameters defining the instance of the VR alg,
                       default values correspond to ATLAS's implementation.
    '''

    R = rho / pt

	# Logic for is pt is passed as a float
    if R.size == 1:
        if R < Rmin: return Rmin
        if R > Rmin: return Rmax

    R[R < Rmin] = Rmin
    R[R > Rmax] = Rmax

    return R

def ftag_cone(pt):
    '''
    - pt in [GeV]
    '''
    pt_mev = pt * 1e3
    return 0.239 + np.exp(-1.22 - 1.64e-5 * pt_mev)
